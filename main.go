package main

import (
	"log"
	"net/http"
	"os"
	"time"

	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

func main() {
	c, err := NewConfig()
	if err != nil {
		log.Fatalf("could NOT initialize configuration: %s", err)
	}

	cbURL, err := c.CallbackURL()
	if err != nil {
		log.Fatalf("error when generating callback URL: %s", err)
	}

	defaultTimeout := 5
	client := &http.Client{
		Timeout: time.Duration(defaultTimeout) * time.Second,
	}

	meta, err := metadata.FetchV2Metadata(client, c.MetadataEndpoint)
	if err != nil {
		log.Fatalf("could not fetch metadata: %s", err)
	}

	req, err := http.NewRequest(http.MethodGet, cbURL, nil)
	if err != nil {
		log.Fatalf("could not create callback request: %s", err)
	}

	// Add exit_code as query parameter
	q := req.URL.Query()
	q.Add("cluster", meta.Cluster)
	q.Add("task_arn", meta.TaskARN)
	q.Add("family", meta.Family)
	q.Add("revision", meta.Revision)
	q.Add("desired_status", meta.DesiredStatus)
	q.Add("known_status", meta.KnownStatus)
	q.Add("pull_started_at", meta.PullStartedAt)
	q.Add("pull_stopped_at", meta.PullStoppedAt)
	q.Add("execution_stopped_at", meta.ExecutionStoppedAt)
	q.Add("availability_zone", meta.AvailabilityZone)
	req.URL.RawQuery = q.Encode()

	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("callback request failed: %s", err)
	}
	defer res.Body.Close()

	os.Exit(0)
}
