module gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	gitlab.com/czerasz/go-aws-ecs-metadata v0.2.0
)
