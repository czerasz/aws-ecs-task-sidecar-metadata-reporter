# AWS ECS Task Sidecar Metadata Reporter

Need to report the task ARN (or other details) when an AWS ECS task starts?

If a HTTP service needs to be notified when the AWS ECS task has started, then this repository and the related Docker image can help. Simply run a sidecar container next to the main container.

## Usage

Example container definitions:

```json
{
  "name": "sidecar-init",
  "image": "registry.gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter:v0.1.1",
  "essential": false,

  "environment": [
    {
      "name": "CALLBACK_URL",
      "value": "${callback_url}"
    }
  ],
  ...
},
{
  "name": "pipeline",
  "essential": true,

  "dependsOn": [
    {
      "containerName": "sidecar-init",
      "condition": "SUCCESS"
    }
  ],
  ...
}
```

## Overview

![Overview](docs/images/overview.png)

An ECS cluster runs the task. Inside the main container, assisted by a sidecar container.
The sidecar container fetches the AWS ECS task metadata, sends it to the specified callback URL and then stops.

The following query parameters are attached to the callback URL:

- `cluster`
- `task_arn`
- `family`
- `revision`
- `desired_status`
- `known_status`
- `pull_started_at`
- `pull_stopped_at`
- `execution_stopped_at`
- `availability_zone`

## Environment Variables

| Environment Variable Name                | Type      | Required | Default                | Description |
| ---------------------------------------- | --------- | -------- | ---------------------- | ----------- |
| `CALLBACK_URL`                           | `string`  | Yes      | -                      | The URL which should be called |
| `CALLBACK_USER`                          | `string`  | No       | -                      | Basic auth user for the callback URL |
| `CALLBACK_PASSWORD`                      | `string`  | No       | -                      | Basic auth password for the callback URL |
| `AWS_ECS_TASK_METADATA_ENDPOINT`         | `string`  | No       | `http://169.254.170.2` | AWS ECS task metadata endpoint base URL |
| `AWS_ECS_TASK_METADATA_ENDPOINT_VERSION` | `string`  | No       | `v2`                   | AWS ECS task metadata endpoint API version |
