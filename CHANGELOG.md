## [0.1.1](https://gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter/compare/v0.1.0...v0.1.1) (2020-07-30)


### Code Refactoring

* adjust query parameter names ([42390aa](https://gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter/commit/42390aa90b5e489dcef0b23b0afc18a8c4de7c7d))


### Documentation

* add documentation ([07fac84](https://gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter/commit/07fac84054923583f763e11f6c5638d2c128ea89))
