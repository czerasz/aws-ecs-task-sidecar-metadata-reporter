package main_test

import (
	"os"
	"testing"

	main "gitlab.com/czerasz/aws-ecs-task-sidecar-metadata-reporter"
	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

func TestNewConfig(t *testing.T) {
	var c *main.Config
	var err error

	os.Unsetenv("CALLBACK_URL")
	c, err = main.NewConfig()
	if err == nil {
		t.Logf("url: %s", c.ClbURL)
		t.Errorf("when CALLBACK_URL is unset the config should fail")
		return
	}

	os.Setenv("CALLBACK_URL", "http://localhost:8081/callback")
	c, err = main.NewConfig()
	if err != nil {
		t.Errorf("config should not fail")
		return
	}

	equals(t, c.ClbURL, "http://localhost:8081/callback")
	equals(t, c.MetadataEndpointVersion, metadata.AwsECSTaskMetadataEndpointVersion)
}

func TestCallbackURL(t *testing.T) {
	var c *main.Config
	var err error

	os.Setenv("CALLBACK_URL", "http://localhost:8081/callback")
	c, err = main.NewConfig()
	if err != nil {
		t.Errorf("unexpected error: %s", err)
		return
	}

	url, err := c.CallbackURL()
	if err != nil {
		t.Errorf("unexpected error: %s", err)
		return
	}
	equals(t, url, "http://localhost:8081/callback")
}
